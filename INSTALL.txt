
Installation
=================================

The restore module provides the core functionality which is required by all
modules.

The "Script file" module provides the ability to use script files
as well as hooks.

The "Script UI" module provides a UI for creating and editing scripts.

The "Script export" module provides the ability to export a restore script
to a given format (hook or file).

There is an example module that shows how to use the restore script hook.

There are also several restore operation modules which allows other modules
to allow restore to export/import settings.

The restore scripts can be viewed by going to:
    Admin > Configuration > Development > Restore
