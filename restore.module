<?php
/**
 * @file
 * The core file of the restore functionality.
 */

define('RESTORE_MENU_PATH', 'admin/config/development/restore');

define('RESTORE_PERM_ACCESS', 'access restore scripts');
define('RESTORE_PERM_RUN', 'run restore scripts');

/**
 * Implements hook_menu().
 */
function restore_menu() {
  $items = array();

  $items[RESTORE_MENU_PATH] = array(
    'title' => 'Restore',
    'description' => 'Available restore scripts.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('restore_ui_list'),
    'access arguments' => array(RESTORE_PERM_ACCESS),
    'file' => 'ui/list.ui.inc',
  );

  $items[RESTORE_MENU_PATH . '/list'] = array(
    'title' => 'Restore scripts',
    'description' => 'List available restore scripts.',
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items[RESTORE_MENU_PATH . '/script/%restore'] = array(
    'title callback' => array('restore_script_title'),
    'title arguments' => array('view', 5),
    'description' => 'View the current script.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('restore_ui_script_view', 5),
    'access arguments' => array(RESTORE_PERM_ACCESS),
    'file' => 'ui/view.ui.inc',
  );

  $items[RESTORE_MENU_PATH . '/script/%restore/view'] = array(
    'title' => 'View',
    'weight' => -10,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function restore_permission() {
  $items = array();

  $items[RESTORE_PERM_ACCESS] = array(
    'title' => RESTORE_PERM_ACCESS,
    'description' => 'Allow access to the restore scripts.',
  );

  $items[RESTORE_PERM_RUN] = array(
    'title' => RESTORE_PERM_RUN,
    'description' => 'Allow running restore scripts.',
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function restore_theme() {
  $items = array();

  $items['restore_ui_list'] = array(
    'render element' => 'form',
    'file' => 'ui/list.ui.inc',
  );

  $items['restore_ui_script_view'] = array(
    'render element' => 'form',
    'file' => 'ui/view.ui.inc',
  );

  return $items;
}

/**
 * Restore script menu title callback.
 */
function restore_script_title($type, $script) {
  return $script ? t('@type script: @script', array(
    '@type' => ucfirst($type),
    '@script' => $script->title(),
  )) : t('Unknown script');
}

/**
 * Load a restore script.
 *
 * @param string $name
 *   The script name.
 *
 * @return RestoreScript|boolean
 *   Returns either the restore script or FALSE.
 */
function restore_load($name) {
  $scripts = &drupal_static(__FUNCTION__);
  if (!isset($scripts[$name])) {
    $restore_scripts = restore_scripts();
    if (!isset($restore_scripts[$name])) {
      return FALSE;
    }

    $scripts[$name] = $restore_scripts[$name];
  }

  return $scripts[$name];
}

/**
 * Import script details either in the form of an array or JSON.
 *
 * @param string|array $json
 *   The JSON string or array.
 * @param string $name
 *   (optional) The name to give this script.
 *
 * @return RestoreScript
 *   Returns the generated restore script.
 */
function restore_import($json, $name = FALSE) {
  if (is_string($json)) {
    $json = json_decode($json, TRUE);
  }

  $script = FALSE;
  if ($json) {
    if ($name === FALSE) {
      $name = isset($json['name']) ? $json['name'] : md5(serialize($json));
    }

    $script = new RestoreScript($name, $json);
  }

  return $script;
}

/**
 * Return all found restore scripts.
 *
 * @return array
 *   An array of restore scripts.
 */
function restore_scripts() {
  $restore_scripts = &drupal_static(__FUNCTION__);

  if (!isset($restore_scripts)) {
    $restore_scripts = array();

    $hook = 'restore_scripts';
    foreach (module_implements($hook) as $module) {
      $function = "{$module}_{$hook}";
      $scripts = call_user_func_array($function, array());

      if ($scripts && count($scripts)) {
        foreach ($scripts as $name => $script) {
          $script['module'] = $module;
          $restore_scripts[$name] = new RestoreScript($name, $script);
        }
      }
    }

    drupal_alter('restore_scripts', $restore_scripts);
  }

  return $restore_scripts;
}

/**
 * Get an operation class.
 *
 * @param string $type
 *   The operation type.
 * @param array $details
 *   The details to pass on to the operation class.
 *
 * @return boolean|RestoreOperation
 *   The operation class, or FALSE.
 */
function restore_operation($type, array $details) {
  $operations = restore_operations();
  if (!isset($operations[$type])) {
    return FALSE;
  }

  if (!class_exists($operations[$type]['class'])) {
    watchdog('restore', 'Failed to find the operation class "@class".', array(
      '@class' => $operations[$type]['class'],
    ), WATCHDOG_ERROR);
  }

  $cls = $operations[$type]['class'];
  return new $cls($operations[$type], $type, $details);
}

/**
 * Return an array of registered operation classes.
 *
 * @return array
 *   An array of operation classnames.
 */
function restore_operations() {
  $operations = &drupal_static(__FUNCTION__);
  if (!isset($operations)) {
    $operations = module_invoke_all('restore_operations');
  }

  drupal_alter('restore_operations', $operations);
  return $operations;
}

/**
 * Implements hook_restore_operations().
 */
function restore_restore_operations() {
  $items = array();

  $items['modules'] = array(
    'title' => t('Modules'),
    'description' => t('Enable or Disable modules.'),
    'class' => 'RestoreOperationModules',
  );

  $items['variables'] = array(
    'title' => t('Variables'),
    'description' => t('Store variables.'),
    'class' => 'RestoreOperationVariables',
  );

  $items['dateformats'] = array(
    'title' => t('Date formats'),
    'description' => t('Custom date formats.'),
    'class' => 'RestoreOperationDateFormats',
  );

  $items['datetypes'] = array(
    'title' => t('Date types'),
    'description' => t('Custom date types.'),
    'class' => 'RestoreOperationDateTypes',
  );

  $items['roles'] = array(
    'title' => t('Roles'),
    'description' => t('User roles.'),
    'class' => 'RestoreOperationRoles',
  );

  $items['permissions'] = array(
    'title' => t('Permissions'),
    'description' => t('Role permissions.'),
    'class' => 'RestoreOperationPermissions',
  );

  $items['image_styles'] = array(
    'title' => t('Image styles'),
    'description' => t('Image styles.'),
    'class' => 'RestoreOperationImageStyles',
  );

  $items['format_filters'] = array(
    'title' => t('Format filters'),
    'description' => t('Format filters.'),
    'class' => 'RestoreOperationFormatFilters',
  );

  return $items;
}

// Include the helper functions.
include_once 'restore.helpers.inc';
